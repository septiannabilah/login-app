package com.example.asus.loginapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText Name;
    private EditText Password;
    private Button SignIn;
    // private Button Register;
    private TextView Register;
    private int Attempt = 5;
    public String InputName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set layout screen with no tittle bar attached on it
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        setContentView(R.layout.activity_main);

        Name = (EditText)findViewById(R.id.fillName);
        InputName = Name.getText().toString();
        Password = (EditText)findViewById(R.id.fillPass);
        SignIn = (Button)findViewById(R.id.btnSignIn);
        Register = (Button)findViewById(R.id.btnReg);

        SignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Validate(Name.getText().toString(), Password.getText().toString());
            }
        });

        Register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegistrationPage.class);
                startActivity(intent);
            }
        });

    }

    private void Validate(String userName, String userPass){
        if ((userName.equals("Septian")) && (userPass.equals("09091999"))){
            Intent intent = new Intent(MainActivity.this, HomePage.class);
            startActivity(intent);
        }
        else {
            Attempt -= 1;

            if (Attempt <= 0){
                SignIn.setEnabled(false);
            }
        }
    }
}
